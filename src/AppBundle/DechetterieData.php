<?php


namespace App\AppBundle;


use App\Model\Dechetterie;
use GuzzleHttp\Client;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DechetterieData
{
    private $serializer;
    private $dechetterieClient;

    /**
     * DechetterieData constructor.
     * @param $serialize
     * @param $dechetterieClient
     */
    public function __construct()
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
        $this->dechetterieClient = new Client();
    }

    public function getCurrent(){
        $uri = "https://opendata.bordeaux-metropole.fr/api/records/1.0/search/?dataset=dechetteries-en-temps-reel&q=&rows=1000&facet=statut&facet=insee";
        $reponse = $this->dechetterieClient->get($uri);
        $reponse = json_decode($reponse->getBody()->getContents())->records;
        $data =array();
        foreach ($reponse as $dechetterie){
            $dechetterie->fields->coordonnees = $dechetterie->fields->geo_point_2d;
            unset($dechetterie->fields->geo_point_2d);
            array_push($data, $this->serializer->deserialize(json_encode($dechetterie->fields), Dechetterie::class, 'json'));
        }
        return $data;
    }
}