<?php


namespace App\AppBundle;


use App\Model\Composteur;
use GuzzleHttp\Client;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ComposteurData
{
    private $serializer;
    private $composterClient;

    /**
     * DechetterieData constructor.
     * @param $serialize
     * @param $dechetterieClient
     */
    public function __construct()
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
        $this->composterClient = new Client();
    }

    public function getCurrent(){
        $uri = "https://opendata.bordeaux-metropole.fr/api/records/1.0/search/?dataset=en_composteur_p&q=&rows=1000&facet=insee&facet=typologie_site&facet=statut";
        $reponse = $this->composterClient->get($uri);
        $reponse = json_decode($reponse->getBody()->getContents())->records;
        $data =array();
        foreach ($reponse as $composteur){
            $composteur->fields->coordonnees = $composteur->fields->geo_point_2d;
            $composteur->fields->photo = $composteur->fields->url_photo;
            unset($composteur->fields->geo_point_2d);
            array_push($data, $this->serializer->deserialize(json_encode($composteur->fields), Composteur::class, 'json'));
        }
        return $data;
    }
}