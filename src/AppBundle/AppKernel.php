<?php

namespace App\AppBundle;

use Csa\Bundle\GuzzleBundle\CsaGuzzleBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundle = [
            new CsaGuzzleBundle()
        ];
        return $bundle;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        // TODO: Implement registerContainerConfiguration() method.
    }
}