<?php


namespace App\Model;


class Composteur
{
    private $gid;
    private $libelle;
    private $statut;
    private $adresse;
    private $photo;
    private $coordonnees;


    /**
     * Composteur constructor.
     * @param $libelle
     * @param $statut
     * @param $adresse
     * @param $url_photo
     */
    public function __construct($libelle, $statut, $adresse, $photo, $gid, $coordonnees)
    {
        $this->libelle = $libelle;
        $this->statut = $statut;
        $this->adresse = $adresse;
        $this->photo = $photo;
        $this->gid = $gid;
        $this->coordonnees = $coordonnees;
    }

    /**
     * @return mixed
     */
    public function getCoordonnees()
    {
        return $this->coordonnees;
    }

    /**
     * @param mixed $coordonnees
     */
    public function setCoordonnees($coordonnees): void
    {
        $this->coordonnees = $coordonnees;
    }


    /**
     * @return mixed
     */
    public function getGid()
    {
        return $this->gid;
    }

    /**
     * @param mixed $gid
     */
    public function setGid($gid): void
    {
        $this->gid = $gid;
    }


    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle): void
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param mixed $statut
     */
    public function setStatut($statut): void
    {
        $this->statut = $statut;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse): void
    {
        $this->adresse = $adresse;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo): void
    {
        $this->photo = $photo;
    }
}