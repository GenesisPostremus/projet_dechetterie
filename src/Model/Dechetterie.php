<?php


namespace App\Model;


class Dechetterie
{
    private $nom;
    private $ident;
    private $statut;
    private $acceptes;
    private $coordonnees;
    private $adresse;
    private $refuses;

    /**
     * Dechetterie constructor.
     * @param $nom
     * @param $statut
     * @param $acceptes
     * @param $coordonnees
     * @param $refuses
     */
    public function __construct($nom, $statut, $acceptes, $coordonnees, $refuses, $adresse, $ident)
    {
        $this->nom = $nom;
        $this->statut = $statut;
        $this->acceptes = $acceptes;
        $this->coordonnees = $coordonnees;
        $this->refuses = $refuses;
        $this->adresse = $adresse;
        $this->ident = $ident;
    }

    /**
     * @return mixed
     */
    public function getIdent()
    {
        return $this->ident;
    }

    /**
     * @param mixed $ident
     */
    public function setIdent($ident): void
    {
        $this->ident = $ident;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse): void
    {
        $this->adresse = $adresse;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param mixed $statut
     */
    public function setStatut($statut): void
    {
        $this->statut = $statut;
    }

    /**
     * @return mixed
     */
    public function getAcceptes()
    {
        return $this->acceptes;
    }

    /**
     * @param mixed $acceptes
     */
    public function setAcceptes($acceptes): void
    {
        $this->acceptes = $acceptes;
    }

    /**
     * @return mixed
     */
    public function getCoordonnees()
    {
        return $this->coordonnees;
    }

    /**
     * @param mixed $coordonnees
     */
    public function setCoordonnees($coordonnees): void
    {
        $this->coordonnees = $coordonnees;
    }

    /**
     * @return mixed
     */
    public function getRefuses()
    {
        return $this->refuses;
    }

    /**
     * @param mixed $refuses
     */
    public function setRefuses($refuses): void
    {
        $this->refuses = $refuses;
    }

}