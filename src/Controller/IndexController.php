<?php

namespace App\Controller;

use App\AppBundle\ComposteurData;
use App\AppBundle\DechetterieData;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(): Response
    {
        $dechetterie_data = new DechetterieData();
        $composteur_data = new ComposteurData();
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
            'dataDechetterie' => $dechetterie_data->getCurrent(),
            'dataComposteur' => $composteur_data->getCurrent()
        ]);
    }
}
